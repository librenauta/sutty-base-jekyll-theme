---
title: "Tema"
font_family_sans_serif: ""
headings_font_family: ""
enable_rounded: false
enable_shadows: false
body_bg: "#FFFFFF"
body_color: "#212529"
primary: "#007bff"
secondary: "#6c757d"
link_color: "#007bff"
link_hover_color: "#0056b3"
h1_font_size: "2.5rem"
h2_font_size: "2rem"
h3_font_size: "1.75rem"
h4_font_size: "1.5rem"
h5_font_size: "1.25rem"
h6_font_size: "1rem"
mark_bg: "#fcf8e3"
navbar_light_color: "#ced4da"
navbar_light_hover_color: "#6c757d"
navbar_light_active_color: "#212529"
testing: false
order: 1
draft: false
layout: "theme"
uuid: "a308d07e-5426-4aca-bd27-c847ddc11c42"
liquid: false
---

